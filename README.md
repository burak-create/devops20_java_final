# Devops20_java_final

## Betygskriterier

### För Godkänd (G) krävs att studenten får godkänt på följande kriterier

* Studenten kan använda verktyg för att lösa enklare programmeringsuppgifter i Java
* Studenten förstår principerna bakom programexekvering samt kan skriva enkla, välstrukturerade och väldokumenterade program i Java
* Studenten kan använda de viktigaste delarna av kodbiblioteket som ingår i Java SE samt använda något verktyg för versionshantering, både för programmering enskilt och i team
* Studenten kan läsa och felsöka Javakod och tillämpa testdriven utveckling (TDD)

### För Väl Godkänd (VG) krävs att studenten får godkänt på samtliga G – respektive VG-kriterier

* Studenten självständigt gör motiverade val för att hålla en god struktur i sin programkod
* Studenten visar god förmåga i att lösa enklare programmeringsuppgifter med hjälp av Java på egen hand
* Studenten visar god förståelse för versionshantering av programkod.
* Studenten självständigt applicerar TDD på individuella eller grupp-projekt

### För VG är det extra noga med

* Ren och tydlig kod
* Bra variabelnamn enligt Java konventioner
* Korta tydliga metoder som gör en isolerad sak.
* Objektorienterad kod används och byggs med egna välavvägda klasser där det behövs.
* Koden är väl testad, med bra testdata.

## Instruktioner

### Regler

* Slutuppgiften är enskild
* Det är ok att fråga lärare och varandra generella frågor.
* Ni ska skriva er egen kod, om ni använder er av kodbitar från webben
  t.ex. stackoverflow så måste ni ange källa.
* Ni ansvarar för att Betygskriterierna är uppfyllda, kolla er kursplan.

### Att arbeta i GitLab och Git

* Gör en fork av detta repository
* OBS ni ska alltså clona eran fork, inte detta repo.
* När ni gör en fork så försvinner rättigheterna. Innan ni börjar arbeta, lägg till läraren som `maintainer`.
* Skapa en branch som ni arbetar i.

### Inlämning 24/1 23:59

* Deadline Söndag 24/1 23:59, merga in alla dina Merge Request mot `master` i forken, dvs INTE mot lärarens repo.
* Dubbelkolla att programmet fungerar, ni kan t.ex. clona repot i en ny folder och testa er branch där.
* Om ni har missat något finns en extra chans på seminariet att visa vad ni kan, var förberedda.
* Seminariet äger rum i små grupper Måndag 25/1.

### Att välja uppgift

* Skriv till mig vilken uppgift ni önskar.
* Det finns ingen garanti att alla kan få den uppgift de vill, så skriv också ett andrahandsval

## Uppgifter

### Platsbokning på flyget

I denna uppgift ska du skriva ett platsbokningsprogram. Resenären ska kunna välja en plats från en lista eller konsolbild bild av planet. Där varje plats identifieras med radnummer och en bokstav. Se t.ex. [norwegian platsöversikt](https://www.norwegian.com/se/reseinformation/ombord/platsreservation/platsoversikt/). Det räcker om programmet stödjer ett litet plan som har exempelvis 20 platser.

#### Platsbokning - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du kan lista / visa alla platser med status BOKAD/LEDIG
* Du kan välja en ledig plats
* Du kan spara ner programmets data till en fil eller databas
* Du kan läsa upp programmets data från en fil eller databas
* Du kan skriva ut en passagerarlista med alla passagerare på planet, listan innehåller deras namn och födelsedatum

#### Platsbokning - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan boka en slumpvis ledig plats
* Din lösning visar även priset på biljetten
* Du kan söka på egenskaper, såsom extra benutrymme
* Du kan boka flera platser samtidigt, där totalt pris framgår
* Du kan ha flera flygningar

### Kafeterian

I denna uppgift ska du skriva ett system som hanterar beställningar till baristan. Kunderna har många olika önskemål som systemet måste hantera. Se t.ex. [wikipedia om kaffedrycker](https://sv.wikipedia.org/wiki/Kaffe#Kaffedrycker)

#### Kafeterian - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du kan lista / visa 8 st kaffedrycker
* Du kan välja om du vill ha extra socker och/eller mjölk
* Du kan spara ner menyn till en fil eller databas
* Du kan läsa upp menyn från en fil eller databas
* Du kan skriva ut alla nuvarande beställningar

#### Kafeterian - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan boka en slumpvis vald kaffe
* Du kan sparar ner alla beställningar till fil eller databas
* Du kan markera en beställning mottagen, klar, serverad
* Du kan välja sockerfritt, koffeinfritt, laktosfritt
* Du kan hantera olika pris på kaffet

### Netflix analyseraren

I denna uppgift ska du skriva ett program som undersöker din netflix användning. *OBS* denna uppgift kräver tillgång till ett netflix konto med historisk använding. Se [så här gör du för att granska och ladda ner streamingaktivitet](https://help.netflix.com/sv/node/101917)

#### Netflix analyseraren - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du kan läsa upp datat från en netflix csv fil
* Du kan skriva ut det senaste som visades på netflix.
* Du kan lista exempelvis de 10 senaste visningarna
* Du kan söka på namn och summera antalet träffar
* Du kan spara din visade data till en fil eller databas

#### Netflix analyseraren - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan slumpa fram en visning från historiken
* Du kan ta fram en lista sorterat på antal visningar
* Du kan ta fram en lista sorterat på antalet säsonger du tittat på för en specifik serie
* Du kan ta fram totalt antal visningar
* Du kan filtrera din sökning med hjälp av datum.

### Mat beställaren

I denna uppgift ska du skriva ett program som hanterar matbeställningar.

#### Mat beställaren - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du kan läsa upp varor från en fil eller databas (minst 10 varor)
* Du kan skriva ut alla varor i sortimentet
* Du kan lägga till en vara i varukorgen
* Du kan ta bort en vara från varukorgen
* Du kan skicka din beställning
* Dina beställningar sparas till en fil eller databas

#### Mat beställaren - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan lägga en slumpvis vara till varukorgen
* Du kan ta fram en lista över historiska beställningar sorterat på totalbelopp
* Du kan ta fram en lista över historiska beställningar sorterat på datum
* Du kan hantera flera kunder
* Du kan ta fram lista över kunder sorterat på totalt belopp av alla köp.

### Platsbokning på konferensanläggningen

I denna uppgift ska du skriva ett platsbokningsprogram. Ert konsultbolag ska kunna välja en lokal baserat på kriterier. Där varje lokal identifieras med ett namn. Det räcker om programmet stödjer 10 lokaler.

#### Konferensanläggningen - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du kan lista / visa alla lokaler med status BOKAD/LEDIG
* Du kan välja att boka en ledig lokal
* Du kan spara ner bokningar till en fil eller databas
* Du kan läsa upp bokningar från en fil eller databas
* Du kan skriva ut en bokningslista, listan innehåller företagsnamn och kontaktperson

#### Konferensanläggningen - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan söka på t.ex. projektor eller bastu för att hitta lämplig lokal
* Din lösning visar även priset för lokalens bokning
* Du kan boka flera platser samtidigt, där totalt pris framgår samt 10% rabatt
* Du kan lista ett företags bokningar

### Snöröjning och halkbekämpning

I denna uppgift ska du skriva ett program som hanterar kommunens snöröjning och halkbekämpning. Programmet arbetar per dag, så du anger - eller + på snödjup per dag, du får även avgöra om snöröjning ska beställas.

#### Snöröjning och halkbekämpning - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du börjar varje dag med att visa och ange om snödjupet har ökat eller minskat
* Du kan beställa snöröjning, då sänks snödjupet till 0
* Du kan bestämma om det ska halkbekämpas
* Du kan söka på när halkbekämpning senast gjordes
* Du kan visa historiskt snödjup per dag
* Du kan skriva ut historik till fil eller databas

#### Snöröjning och halkbekämpning - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan läsa historisk data från fil eller databas
* Du kan se en uppskattning för morgondagens förändring av snödjup
* Du kan ange vilken utförare/entreprenör du vill anlita för jobbet

### Bokhandeln

I denna uppgift ska du skriva ett program som hanterar köp av böcker.

#### Bokhandeln - Krav G

* Du har enhetstester för de viktigaste metoderna.
* Du kan lista tillgängliga böcker (minst 10 st)
* Du kan söka bok på titel
* Du kan söka bok på ISBN-nummer
* Du kan söka bok på författare
* Du kan lägga till bok i varukorg
* Du kan se totalsumma för varukorg
* Du kan slutföra beställningen

#### Bokhandeln - Krav VG

* Du har heltäckande enhetstester och har uttänkt testdata.
* Du kan se lagerstatus böcker
* Du kan reservera en bok som är slut i lager
* Du kan önska en ny bok som inte finns i butiken
* Du använder en databas eller fil för att spara böcker och köp
